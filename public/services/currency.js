(function () {

    'use strict';

    angular
        .module('currencyApp')
        .factory('currency', currency);

    function currency($http) {
        
        function getExchangeRate(baseId, targetId, onExchangeRateReady){
            $http({
                method: "GET",
                url: " https://api.fixer.io/latest?base="+baseId+"&symbols="+targetId
            }).then(function mySucces(response) {

                var responseData = response.data;
                onExchangeRateReady(eval("responseData.rates."+targetId));

            }, function myError(response) {
                scope.has_error = true;
                scope.error_message = response.message;
                return null;
            });
        }

        function getCurrencies() {
            
            return [
                {symbol:"AUD", country: "Australian Dollars"},
                {symbol:"BRL", country: "Brazilian Reals"},
                {symbol:"BGN", country: "Bulgarian Levs"},
                {symbol:"CAD", country: "Canadian Dollars"},
                {symbol:"CNY", country: "Chinese Yuan Renminbi"},
                {symbol:"HRK", country: "Croatian Kunas"},
                {symbol:"CZK", country: "Czech Koruna"},
                {symbol:"DKK", country: "Danish Kroner"},
                {symbol:"EUR", country: "Euros"},
                {symbol:"HKD", country: "Hong Kong Dollar"},
                {symbol:"HUF", country: "Hungarian Florins"},
                {symbol:"INR", country: "Indian Rupee"},
                {symbol:"IDR", country: "Indonesian Rupiah"},
                {symbol:"ILS", country: "Israeli Shekel"},
                {symbol:"JPY", country: "Japanese Yen"},
                {symbol:"KRW", country: "Korean Won"},
                {symbol:"MYR", country: "Malaysian Ringgits"},
                {symbol:"MXN", country: "Mexican Pesos"},
                {symbol:"NZD", country: "New Zealand Dollar"},
                {symbol:"NOK", country: "Norwegian Kroner"},
                {symbol:"PHP", country: "Philippine Pesos"},
                {symbol:"PLN", country: "Polish Zloty"},
                {symbol:"RON", country: "Romanian Leus"},
                {symbol:"RUB", country: "Russian Rubles"},
                {symbol:"SGD", country: "Singapore Dollars"},
                {symbol:"ZAR", country: "South African Rand"},
                {symbol:"SEK", country: "Swedish Krona"},
                {symbol:"CHF", country: "Swiss Francs"},
                {symbol:"THB", country: "Thai Baht"},
                {symbol:"TRY", country: "Turkish Lira"},
                {symbol:"GBP", country: "UK Pounds"},
                {symbol:"USD", country: "US Dollars"}
            ];
        }

        return {
            getCurrencies: getCurrencies,
            getExchangeRate: getExchangeRate
        }
    }

})();