/**
 *  @Copyright Maziar Navabi March 2017
 *  calculates echange from one currency to another
 *  //todo: fix data validtion to only accept numbers
 *  //todo: show error mesage if connection is lost
 */
(function() {
    'use strict';
    angular
        .module('currencyApp')
        .controller('CurrencyController', CurrencyController);

    function CurrencyController($scope, $http, currency ) {


        $scope.currencies = [];
        $scope.exchangeRate = 1;
        $scope.inputValue=0;
        $scope.outputValue=0;


        $scope.loadCurrencySymbols = function() {
            $scope.from_currencies= currency.getCurrencies();
            $scope.to_currencies= $scope.from_currencies;
        };

        $scope.exchangeRateReady=function(fetchedExchangeRate){
            $scope.exchangeRate=fetchedExchangeRate;
            $scope.updateResult();

        };

        $scope.onCurrencyFromChange = function () {
            var baseId=$scope.selectedCurrencyFromId;
            var targetId=$scope.selectedCurrencyToId;
            $scope.outputValue="calculating...";
            currency.getExchangeRate(baseId, targetId, $scope.exchangeRateReady);
        };

        $scope.onCurrencyToChange = function () {
            var baseId=$scope.selectedCurrencyFromId;
            var targetId=$scope.selectedCurrencyToId;
            $scope.outputValue="calculating...";
            currency.getExchangeRate(baseId, targetId, $scope.exchangeRateReady);
        };

        $scope.updateResult=function(){
            $scope.outputValue= Math.round($scope.inputValue* $scope.exchangeRate*100) / 100;
        };

        $scope.loadCurrencySymbols();
    }

})();