(function () {

    'use strict';

    angular
        .module('currencyApp', ['ui.router'])
        .config(function ($stateProvider, $httpProvider) {

            $stateProvider
                .state('currency-exchange', {
                    url: '/currency-exchange',
                    templateUrl: './views/domain/currency-exchange/currency-exchange.html',
                    controller: 'CurrencyController as ctrl',
                })
                .state('index.currency-exchange', { 
                    url: '/',
                    views: {
                        'left@index': {
                            templateUrl: './views/domain/currency-exchange/sides/list.html',
                            controller: 'DummyCtrl'
                        },
                        'main@index': {
                            templateUrl: './views/domain/currency-exchange/currency-exchange.html',
                            controller: 'CurrencyController as ctrl'
                        },
                    },
                })
        })
        .controller('DummyCtrl', function () {
        })
        .run(function ($rootScope, $state) {

        });
})();