var gulp = require('gulp'),
	bower = require('gulp-bower');

var pump = require('pump');
var config = {
     bowerDir: './lib'
}
 
gulp.task('bower', function() {
    return bower()
        .pipe(gulp.dest(config.bowerDir))
});

gulp.task('default', ['bower']);